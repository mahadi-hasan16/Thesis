<h2>Title:</h2><h3> Level Classification of Cognitive Load Using Machine Learning.</h3><br>
<h2>Abstract:</h2><br>
Cognitive load refers to the mental demand placed on working memory during a task. It makes use
of an information processing model to explain how the mind gathers and stores knowledge as well
as the constraints placed on working memory. Accurately classifying cognitive load levels has a
great impact in various fields, including education, human-computer interaction, and workload
management. This study looks at the classification of cognitive load using machine learning
methods. The main objective of this study is to categorize the levels of cognitive load using
characteristics taken from physiological signals such as electrocardiogram (ECG) and perceptual
tasks. To do this at first K-Means algorithm, an unsupervised clustering model is used and trained
it by the augmented dataset for generating three clusters. By analyzing the perceptual and
physiological features these clusters are labeled as Low, Medium and High levels of cognitive
load. The results of this research will help to build dependable and effective machine learning
techniques for classifying cognitive load.
|<br><br>

# Dataset
[DATABASE FOR COGNITIVE LOAD AFFECT AND STRESS RECOGNITION](https://ieee-dataport.org/open-access/database-cognitive-load-affect-and-stress-recognition)

# Methodology Overview
![framework](/uploads/5c07f7754181b7d2c34f86d5d6ee0ddc/framework.png)

# Data Preprocessing

## Success Rate Calculation
'Success Rate.py' is used to calculate the Success Rate-
<br><br>
![Success_Rate_Calculation](/uploads/8e272119782e74e1c5148647d7c43fda/Success_Rate_Calculation.png)
<br><br>
The Bar Chart of the Succes Rate

![bar_chart_of_SR](/uploads/5461a2017a82534a691f6bad4b53a29d/bar_chart_of_SR.png)

![success_rate_vs_cluster](/uploads/8b5ef696c0d1101224910880f01e3677/success_rate_vs_cluster.png)

![After_Applying_the_Butterworth_Filter](/uploads/c6318e22325bb45a0556e41d64c16703/After_Applying_the_Butterworth_Filter.png)

![bar_chart_of_SR](/uploads/5461a2017a82534a691f6bad4b53a29d/bar_chart_of_SR.png)

![clusters](/uploads/d89a59a7e9b4a4b7a4e95341de9e533f/clusters.png)

![ECG_Signal_after_Applying_the_Notch_Filter](/uploads/9e123d24a3b8532dbeac2db17e45d3eb/ECG_Signal_after_Applying_the_Notch_Filter.png)

![ECG_Signal_before_Applying_the_Notch_Filter](/uploads/7df88d1e7e8d4030519cf5249d04b7e8/ECG_Signal_before_Applying_the_Notch_Filter.png)

![ECG_Summary](/uploads/fccef94a2e65652c3350f93e69131673/ECG_Summary.png)

![EDA](/uploads/d66ef56585d785e26c0e26d919890c54/EDA.png)

![Filtered_VS_Not_iltered_ECG_Signal](/uploads/b72c1b2052eff9d277f8439842024a76/Filtered_VS_Not_iltered_ECG_Signal.png)

![framework](/uploads/5c07f7754181b7d2c34f86d5d6ee0ddc/framework.png)

![histogram_of_SR](/uploads/25fc77cc165053e7a4e09abf04069b69/histogram_of_SR.png)

![IQ_MeanNN_VS_Cluster](/uploads/7f4dd7da621184900132ec1dff3fbd85/IQ_MeanNN_VS_Cluster.png)

![Math_MeanNN_vs_cluster](/uploads/e780c755c7b28eb08dd5687ca5d94828/Math_MeanNN_vs_cluster.png)

![PCA1_VS_Cluster](/uploads/3bacd453b7292458b15deeec2e9b38f9/PCA1_VS_Cluster.png)

![PCA2_VS_Cluster](/uploads/84eb7d53e2df0fa6747a2aa590c49aa4/PCA2_VS_Cluster.png)